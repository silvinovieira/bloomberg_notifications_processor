#!/usr/bin/python3
# -*- coding: UTF-8 -*-

import os
import sys
from datetime import datetime

from PyPDF2 import PdfFileReader
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import models.base
from models.response import Response
from models.notification import EnhancementNotification
from parsers.enhancement import pdf_to_str_list, parse_pdf_content


def main():
    # TODO add log

    # Connects to db and creates tables if they do not exist
    engine = create_engine('sqlite:///bloomberg_processor.db', echo=True)
    models.base.create_tables(engine)
    Session = sessionmaker(bind=engine)

    # Extracts pdf content
    pdf_filename = sys.argv[1]
    pdf_file = open(pdf_filename, 'rb')
    pdf_reader = PdfFileReader(pdf_file)
    pdf_content = pdf_to_str_list(pdf_reader)
    pdf_file.close()

    # Parse notices and stores them on the db
    notices = parse_pdf_content(pdf_content)

    # Save to file
    filename = os.path.split(pdf_filename)[1]
    filename_no_ext = os.path.splitext(filename)[0]
    txt_filename = 'output/{}.txt'.format(filename_no_ext)
    os.makedirs(os.path.dirname(txt_filename), exist_ok=True)
    with open(txt_filename, 'w') as f:
        for notice in notices:
            if notice.m_status:
                response = Response(notice).text
                f.write(notice.display_format(response))

    enhancement_date = datetime.strptime(filename_no_ext.split('_')[-1], '%Y%m%d').date()
    enhancement = EnhancementNotification(filename=pdf_filename, notices=notices, date=enhancement_date)

    # Save to db
    session = Session()
    session.add_all(notices)
    session.add(enhancement)
    session.commit()

if __name__ == '__main__':
    main()
