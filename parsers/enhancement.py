import re

from models.notice import Notice


last_header_item = 'CONFIDENTIAL: ONLY FOR DISCUSSION WITH BLOOMBERG. '


def pdf_to_str_list(pdf_reader):
    pdf_lines = []
    for page in range(0, pdf_reader.getNumPages()):
        page_lines = pdf_reader.getPage(page).extractText().splitlines()
        index_from = get_first_non_header_index(page_lines)
        for line in page_lines[index_from:]:
            stripped_line = line.strip()
            if stripped_line:
                pdf_lines.append(stripped_line)
    return pdf_lines


def get_first_non_header_index(page_lines):
    if last_header_item in page_lines:
        index = page_lines.index(last_header_item) + 1
    elif last_header_item.strip() in page_lines:
        index = page_lines.index(last_header_item.strip()) + 1
    else:
        index = 0
    return index


def parse_pdf_content(pdf_lines):
    notices = []
    lines = remove_unused_lines(pdf_lines)
    ref_id_indices = [i for i, line in enumerate(lines) if line == 'RefID']
    for i in range(0, len(ref_id_indices) - 1):
        notice_lines = lines[ref_id_indices[i]:ref_id_indices[i + 1]]
        notices.append(parse_notice_lines(notice_lines))
    return notices


def remove_unused_lines(lines):
    return [line for line in lines if not re.match(r'\(dd/mm/yyyy\)|-', line)]


def parse_notice_lines(notice_lines):
    fields = Notice.fields
    attr_dict = {}
    next_index = get_field_index(notice_lines, fields[0])
    for i in range(0, len(fields) - 1):
        try:
            index, next_index = next_index, get_field_index(notice_lines, fields[i + 1])
            field_name = Notice.get_attr_name(fields[i])
            attr_dict[field_name] = ' '.join(notice_lines[index + 1:next_index])
        except ValueError as e:
            print(e, notice_lines, sep='\n')
    return Notice(**attr_dict)


def get_field_index(notice_lines, field):
    if field in notice_lines:
        return notice_lines.index(field)
    else:
        for i in range(0, len(notice_lines) - 1):
            # Broken keys
            lines_concat = ''.join(notice_lines[i:i + 2])
            # Key = 'Bulk Files Affected'
            lines_concat_space = ' '.join(notice_lines[i:i + 2])
            if field in (lines_concat, lines_concat_space):
                notice_lines[i] = field
                notice_lines.pop(i + 1)
                return i

        raise ValueError
