from datetime import datetime

from sqlalchemy import Column, Integer, String, Date, ForeignKey

from models.base import Base


class Notice(Base):
    # TODO change data types for some fields
    # TODO evaluate cost of creating a revision class

    __tablename__ = 'notice'

    id = Column(Integer, primary_key=True)

    # Bloomberg fields
    refid = Column(Integer, nullable=False)
    release_date = Column(Date)
    published_date = Column(Date)
    summary = Column(String)
    category = Column(String)
    products = Column(String)
    bulk_files_affected = Column(String)
    asset_class = Column(String)
    description = Column(String)
    region = Column(String)
    driven_by = Column(String)
    revision_date = Column(Date)
    revision_status = Column(String)
    revision_notes = Column(String)

    # Custom fields
    m_status = Column(String)

    # Relationship
    enhancement_id = Column(Integer, ForeignKey('enhancement.id'))

    # Fields in the same order as the enhancement file
    fields = (
        'RefID',
        'Release Date',
        'Published Date',
        'Summary',
        'Category',
        'Products',
        'Bulk Files Affected',
        'Asset Class',
        'Description',
        'Region',
        'Driven by',
        'Revision Date',
        'Revision Status',
        'Revision Notes'
    )

    @staticmethod
    def get_attr_name(field):
        return field.lower().replace(' ', '_')

    def __init__(self, *args, **kwargs):

        refid = kwargs.pop('refid')
        if refid.endswith(('*', '+')):
            kwargs['m_status'] = refid[-1]
            refid = refid[0:len(refid) - 2]
        kwargs['refid'] = refid

        for key, value in kwargs.items():
            if key.endswith('_date'):
                try:
                    kwargs[key] = datetime.strptime(value, '%d/%m/%Y').date()
                except ValueError:
                    kwargs[key] = None

        super(Notice, self).__init__(*args, **kwargs)

    def display_format(self, response):
        display = (
            '----',
            '*RefID {} ({})*'.format(self.refid, self.release_date),
            self.summary,
            '',
            response,
            ''
        )
        return '\n'.join(display)
