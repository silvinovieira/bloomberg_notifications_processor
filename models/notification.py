from models.base import Base

from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.orm import relationship


class EnhancementNotification(Base):

    __tablename__ = 'enhancement'

    id = Column(Integer, primary_key=True)
    filename = Column(String)
    date = Column(Date)

    notices = relationship("Notice")
